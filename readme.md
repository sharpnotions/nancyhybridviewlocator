NancyHybridViewLocator
---

This library provides an IViewLocator implementation for [Nancy](http://nancyfx.org), which can resolve views either from the filesystem (using the default view locator), or from an embedded resource in an assembly (using a custom function).

## How to use

1. Include a reference to the `SN.NancyHybridViewLocator` library
2. Modify your bootstrapper to use `SN.NancyHybridViewLocator.ViewLocator`
3. Use the `EmbeddedView(string, string)` method to refer to an embedded view from within your modules 

### Bootstrapper example

```
using Nancy;
using Nancy.Bootstrapper

public class MyBootstrap : NancyBootstrapperBase
{
  protected override NancyInternalConfiguration InternalConfiguration
  {
    get
    {
      return NancyInternalConfiguration.WithOverrides(cfg => {
        cfg.ViewLocator = typeof(SN.NancyHybridViewLocator.ViewLocator);
      ]);
    }
  }
}
```

### Module example

```
using Nancy;
using SN.NancyHybridViewLocator;

public FancyModule : NancyModule
{
  public FancyModule()
  {
    Get["/embedded"] = _ => this.EmbeddedView("My.Root.Namespace.views", "embedded.html");
  }
}
```

*Note:* the `EmbeddedView` extension method doesn't do anything too crazy: it sets the module's assembly in the current Nancy Context's `Items` collection, and it concatenates the 2 string methods. You can perform this on your own if you would like to perform other actions, such as passing a model.