﻿using Nancy;
using Nancy.Responses.Negotiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.HybridViewLocator
{
    /// <summary>
    /// Provides an extension to reference a view embedded within a module's assembly
    /// </summary>
    public static class EmbeddedViewModuleExtensions
    {
        /// <summary>
        /// Render an view embedded in this module's assembly.
        /// </summary>
        /// <param name="module">The module being executed</param>
        /// <param name="viewName">The name of the view to find within the namespaces advertised by an <seealso cref="EmbeddedViewLocationsAttribute"/>.</param>
        /// <param name="model">The model to pass along with the view.</param>
        /// <returns>A Nancy <see cref="Negotiator"/> response that will allow the HybridViewLocator to find this view in the assembly.</returns>
        public static Negotiator EmbeddedView(this INancyModule module, string viewName, dynamic model = null)
        {
            module.Context.Items["ViewAssembly"] = module.GetType().Assembly;
            return module.View[viewName, model];
        }

        /// <summary>
        /// Performs the necessary per-request setup to enable response negotiation to an embedded view in this assembly.
        /// </summary>
        /// <param name="module"></param>
        public static void UseEmbeddedViews(this INancyModule module)
        {
            module.Context.Items["ViewAssembly"] = module.GetType().Assembly;
        }
    }
}
