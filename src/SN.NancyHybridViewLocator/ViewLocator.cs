﻿using Common.Logging;
using Nancy;
using Nancy.Conventions;
using Nancy.ViewEngines;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SN.HybridViewLocator
{
    /// <summary>
    /// An <see cref="IViewLocator"/> implementation that can retrieve both embedded and filesystem views.
    /// </summary>
    /// <remarks>
    /// In order to successfully locate assembly views over on-disk views, utilize the EmbeddedVieow extension method
    /// on the INancyModule to return a suitable response.
    /// </remarks>
    public class ViewLocator : IViewLocator
    {
        private readonly Dictionary<string, Func<ViewLocationResult>> _embeddedViewCache = new Dictionary<string, Func<ViewLocationResult>>();
        private readonly IViewLocator _fs;
        private readonly IList<string> _validExtensions;
        private ILog _logger = null;

        /// <summary>
        /// Create a new <see cref="ViewLocator"/> instance.
        /// </summary>
        /// <param name="fsViewLocator">The default filesystem view locator</param>
        /// <param name="viewEngines">The view engines in use by the application</param>
        public ViewLocator(DefaultViewLocator fsViewLocator, IEnumerable<IViewEngine> viewEngines)
        {
            _fs = fsViewLocator;
            _validExtensions = viewEngines
                .SelectMany(ve => ve.Extensions)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Returns the collection of cached views retrieved by this viewlocator
        /// </summary>
        /// <returns>A collection of results which have been cached by the locator</returns>
        public IEnumerable<ViewLocationResult> GetAllCurrentlyDiscoveredViews()
        {
            return _fs.GetAllCurrentlyDiscoveredViews()
                .Concat(_embeddedViewCache.Values.Select(cached => cached()).ToArray());
        }

        /// <summary>
        /// Find the specified view
        /// </summary>
        /// <param name="viewName">The name and path of the view to find</param>
        /// <param name="context">The context of the current request</param>
        /// <returns>A non-null result if the view was found</returns>
        public ViewLocationResult LocateView(string viewName, NancyContext context)
        {
            ViewLocationResult result = null;

            var viewAssembly = context.Items.GetValue("ViewAssembly", null) as System.Reflection.Assembly;

            result = LocateAssemblyView(viewAssembly, viewName);
            
            if(result == null)
            {
                result = _fs.LocateView(viewName, context);
            }

            return result;
        }

        private bool IsCached(string viewName)
        {
            return _embeddedViewCache.ContainsKey(viewName);
        }

        private ViewLocationResult LocateAssemblyView(System.Reflection.Assembly assembly, string viewName)
        {
            ViewLocationResult result = null;

            //short-circuit out to the default view locator if viewName looks like a filesystem path
            if(viewName == null || viewName.IndexOf('/') >= 0)
            {
                return result;
            }

            if (assembly != null)
            {
                //the viewName may not have the required file extension, but that's ok
                var assemblyShortName = assembly.GetName().Name;
                var assemblyQualifiedViewName = string.Format("({0}){1}", assemblyShortName, viewName);

                if (IsCached(assemblyQualifiedViewName))
                {
                    result = _embeddedViewCache[assemblyQualifiedViewName]();
                }
                else
                {
                    var embeddedResources = assembly.GetManifestResourceNames().ToList();
                    if (embeddedResources.Any())
                    {
                        
                        var searchFiles = new List<string>();
                        //include the raw viewName, as it may be a fully qualified resource identifier
                        searchFiles.Add(viewName);

                        //retrieve the view locations attribute
                        var attribute = assembly.GetCustomAttributes(false).OfType<EmbeddedViewLocationsAttribute>().FirstOrDefault();
                        if (attribute != null)
                        {
                            //add the requested view + attribute search path
                            searchFiles.AddRange(attribute.Locations.Select(l => l + "." + viewName));

                            if (!Path.HasExtension(viewName))
                            {
                                //if the assembly attribute is found, add in the specified view locations
                                //creating a permutation for each possible namespace "path" and each valid
                                //filename extension (as defined by available view engines)
                                searchFiles.AddRange(attribute.Locations
                                        //select all the permutations of a given filename + path
                                        .SelectMany(location => GetValidExtensions(viewName).Select(fileName => location + "." + fileName))); 
                            }
                        }

                        //get any found matching view
                        var foundView = searchFiles.FirstOrDefault(resourceName => embeddedResources.Contains(resourceName));

                        if (foundView != null)
                        {
                            using (var resourceStream = assembly.GetManifestResourceStream(foundView))
                            {
                                if (resourceStream != null)
                                {
                                    using (var reader = new StreamReader(resourceStream))
                                    {
                                        var viewText = reader.ReadToEnd();

                                        result = CacheView(assemblyQualifiedViewName, viewName, GetExtension(foundView), viewText);
                                    }
                                }
                            }
                        } else
                        {
                            var searchedFiles = string.Join("\r\n  ", searchFiles);
                            var availableResources = string.Join("\r\n  ", embeddedResources);

                            Logger.DebugFormat("Request for embedded view \"{0}\" from assembly \"{1}\" was not fulfilled because no view was found.\r\nSearched for:\r\n  {2}\r\nIn embedded resources:\r\n  {3}", viewName, assemblyShortName, searchedFiles, availableResources);
                        }
                    } else
                    {
                        Logger.DebugFormat("Request for embedded view \"{0}\" from assembly \"{1}\" was not fulfilled because the assembly has no embedded resources.", viewName, assemblyShortName);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Compute all of the valid filenames for the view based on the extension of the file
        /// and the extensions supported by the loaded view engines.
        /// </summary>
        /// <param name="viewName">The name of the view to check</param>
        /// <returns>An array of valid </returns>
        private string[] GetValidExtensions(string viewName)
        {
            var extension = GetExtension(viewName) ?? string.Empty;
            if (_validExtensions.Contains(extension))
            {
                return new string[] { viewName };
            }
            return _validExtensions.Select(e => viewName + "." + e).ToArray();
        }

        private static string GetExtension(string viewName)
        {
            var ext = Path.GetExtension(viewName);
            if (ext.StartsWith(".") && ext.Length > 1)
            {
                ext = ext.Substring(1);
            }
            return ext;
        }

        private ViewLocationResult CacheView(string assemblyQualifiedName, string viewName, string extension, string viewText)
        {
            var viewFunc = new Func<ViewLocationResult>(() => {
                return new ViewLocationResult(viewName, viewName, extension, () => new StringReader(viewText));
            });
            _embeddedViewCache[assemblyQualifiedName] = viewFunc;
            return viewFunc();
        }

        private ILog Logger
        {
            get
            {
                if(_logger == null)
                {
                    _logger = LogManager.GetLogger<ViewLocator>();
                }
                return _logger;
            }
        }
    }

    internal static class IDictionaryExtensions
    {
        /// <summary>
        /// Retreives a value from the dictonary with the specified default value if no key exists in the dictionary
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="dict"></param>
        /// <param name="keyval"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static V GetValue<K, V>(this IDictionary<K, V> dict, K keyval, V defaultValue)
        {
            V outVal;
            if (!dict.TryGetValue(keyval, out outVal))
            {
                outVal = defaultValue;
            }
            return outVal;
        }
    }
}