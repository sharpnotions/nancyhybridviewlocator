﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.HybridViewLocator
{
    /// <summary>
    /// Specifies the namespaces in this assembly which can be searched for views.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
    public class EmbeddedViewLocationsAttribute : Attribute
    {
        /// <summary>
        /// Create a new <see cref="EmbeddedViewLocationsAttribute"/>, specifying namespaces under which a view can be found.
        /// </summary>
        /// <param name="locations"></param>
        public EmbeddedViewLocationsAttribute(params string[] locations)
        {
            Locations = locations ?? new string[0];
        }

        /// <summary>
        /// The namespaces in which to look for embedded resource views
        /// </summary>
        public string[] Locations { get; private set; }
    }
}
